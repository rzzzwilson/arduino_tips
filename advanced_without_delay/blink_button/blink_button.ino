// define the LED stuff, pin and state
const int LedPin = 13;                        // where the LED is connected
const long FastBlinkPeriod = 500;             // 1/2 second on, 1/2 second off
const long SlowBlinkPeriod = 2000;            // 2 seconds, the other LED period
int led_state = LOW;                          // initial state of LED pin
unsigned long blink_period = FastBlinkPeriod; // the current blink period

// define the button pin
const int ButtonPin = 12;

void blink()
{
  // first, flip the LED state
  if (led_state == LOW)
  {
    led_state = HIGH;
  }
  else
  {
    led_state = LOW;
  }

  // then write new value to the pin and delay
  digitalWrite(LedPin, led_state);
  delay(blink_period);
}

void setup()
{
  Serial.begin(115200);
  
  // set the LED pin mode and set initial state
  pinMode(LedPin, OUTPUT);
  digitalWrite(LedPin, led_state);

  // button uses a PULLUP input pin
  pinMode(ButtonPin, INPUT_PULLUP);
}

void loop()
{
  // check the button, switch the period if so
  if (digitalRead(ButtonPin) == LOW)
  {
    // button pressed, switch the blink period
    if (blink_period == SlowBlinkPeriod)
    {
      blink_period = FastBlinkPeriod;
    }
    else
    {
      blink_period = SlowBlinkPeriod;
    }

    Serial.print("Blink period set to ");
    Serial.println(blink_period);

    // wait until button released
    // if we don't wait we get lots of changes!
    while (digitalRead(ButtonPin) == LOW)
      ;
  }

  // then blink the LED at the saved period
  blink();  // one cycle of blink, turn ON or OFF
}
