# toggle_pin

This is a short example showing how to toggle a digital pin state without having
a state variable to remember the pin state.

The idea is simple: it's possible to read the state of a digital OUTPUT pin.
