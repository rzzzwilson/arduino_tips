//
// Test code to see if the 8266 connect to wifi code
// can report things like "bad SSID" and "bad Password".
//
// Despite there being status codes that flag a "bad Password"
// situation, like:
//     WL_WRONG_PASSWORD
//     STATION_WRONG_PASSWORD
// the current ESP8266 WiFi libraries never return those
// statuses.  The code below assumes that no connection after a
// long period is due to a bad Password.
//

#include <ESP8266WiFi.h>

// credentials to connect to WiFi access point
// test with:
//     good SSID and Password
//     bad SSID
//     bad Password
const char* Ssid = "SSID";		      // replace with your credentials
const char* Password = "PASSWORD";

// period, in milliseconds, to wait for a connect to access point
const unsigned long MILLIS_WAIT = 20000;    // 20 seconds


void setup()
{
  // set up Serial, show we are ready
  Serial.begin(115200);
  delay(2500);    // needed so we don't lose print output
  Serial.printf("\n\nREADY\n\n");

  // connect to access point
  WiFi.begin(Ssid, Password);
  Serial.printf("Connecting to '%s' ", Ssid);

  // code to monitor the connect process
  unsigned long start_conn = millis();  // time at start connect
  unsigned long last_dot = 0;           // time previous dot printed
  int wifi_status;

  while ((wifi_status = WiFi.status()) != WL_CONNECTED)
  {
    unsigned long now = millis();
    
    // every second print a "."
    if (now > (last_dot + 1000))
    {
      last_dot = now;
      Serial.printf(".");      
    }
    
    // if we have waited long enough report error
    if (now > (start_conn + MILLIS_WAIT))
    {
      switch (wifi_status)
      {
        case WL_NO_SSID_AVAIL:
          Serial.printf("\nAccess point '%s' is not available.\n", Ssid);
          return;   // do nothing in loop()
        default:
          Serial.printf("\nPassword is not correct.\n");
          return;
      }
    }

    delay(50);
  }

  // report success, how long it took and the IP address
  float ct = (millis() - start_conn) / 1000;
  Serial.printf("\nConnection established!\n");
  Serial.printf("Took %.1f seconds to connect\n", ct);
  Serial.printf("IP address: ");
  Serial.println(WiFi.localIP());
}

void loop()
{
}
