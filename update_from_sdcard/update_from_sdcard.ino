/*
Update the ESP8266 (Wemos D1 Mini used) from an SD card.

This example code just reloads the same binary repeatedly while updating
a counter which is also saved on the card.
*/
 
#include <SD.h>

const char *Version = "1.11";

const bool Verbose = false;
const char *FirmwareFilename = "firmware.bin";
const char *CountFilename = "counter.dat";

// WeMos D1 esp8266: D8 as standard
const int ChipSelect = SS;

// define some error codes returnd from update_from_sdcard()
const int ErrNoFile = 1;
const int ErrBadUpdate = 2;
 
//----------------------------------------------
// This function is called periodically during the update.
//     curr_size   current number of updated bytes
//     total_size  total size of update in bytes
//----------------------------------------------

void progress_callback(size_t curr_size, size_t total_size)
{
  Serial.printf("Update: %7d of %7d bytes, %5.1f%%\n",
                curr_size, total_size, (float)100*curr_size/total_size);
}
 
//----------------------------------------------
// Update a count variable from the given file and
// returns the original value in the file.
//     filename  the path to the file to use
//
// Creates the file with initial "0" if not there.
//----------------------------------------------

int update_count(const char *filename)
{
  File counter;
  char buffer[32];
  char *chptr = buffer;

  // if file doesn't exist create with 0 value
  if (!SD.exists(filename))
  {
    counter = SD.open(filename, FILE_WRITE);
    counter.printf("1\n");
    counter.close();   
  }

  counter = SD.open(filename);
  while (counter.available())
  {
    *chptr++ = counter.read();
  }
  *chptr = '\0';
  counter.close();

  int count = atoi(buffer);

  counter = SD.open(filename, FILE_WRITE);
  counter.seek(0);
  counter.printf("%d\n", count+1);
  counter.close();

  return count;
}

//----------------------------------------------
// Update firmware from a given file.
//     filename  the name of the file to update from
//     verbose   if "true" show progress/debug prints
//
// Assumes the SD card system is set up.
// If the update was successful, cause a restart.
// Otherwise the function returns an error code:
//     ErrNoFile     no update file found
//     ErrBadUpdate  something wrong with the update
//
// On a successful update the "filename" is removed.
//----------------------------------------------

int update_from_sdcard(const char *filename, bool verbose)
{
  File firmware = SD.open(filename, FILE_READ);

  if (!firmware)
  {
    return ErrNoFile;
  }

  if (verbose)
  {
    Serial.printf("found\n");
    delay(100);
    Update.onProgress(progress_callback);
  }

  Update.begin(firmware.size(), U_FLASH);
  Update.writeStream(firmware);
  if (Update.end())
  {
    // close the update file and remove it
    firmware.close();
    if (verbose)
    {
      Serial.printf("Removing update file '%s'\n", filename);
    }

    if (!SD.remove(filename))
    {
      if (verbose)
      {
        Serial.printf("\nERROR: Couldn't delete update file '%s'\n", filename);
      }
      // but we still fallthrough and restart
    }

    // restart the show
    if (verbose)
    {
      Serial.printf("Resetting to run new code...\n\n");
    }
    delay(100);
    ESP.reset();
  }

  // there was an update error, don't continue just wait here
  return ErrBadUpdate;
}

void setup()
{
  // Open serial communications and wait for port to open:
  Serial.begin(115200);
  delay(4000);
 
  if (Verbose)
  {
    Serial.printf("update_from_sdcard, version %s\n", Version);
    Serial.printf("\nInitializing SD card...");
  }
 
  if (!SD.begin(ChipSelect))
  {
    Serial.printf("initialization failed. Things to check:\n");
    Serial.printf("* is a card inserted?\n");
    Serial.printf("* is your wiring correct?\n");
    Serial.printf("* did you change ChipSelect pin to match your module?\n");
    while (1);
  }

  if (Verbose)
  {
    Serial.printf("wiring is correct and a card is present.\n");
  }

// don't use the counter file, code left as example
  // int count = update_count(CountFilename);
  // Serial.printf("Doing update %d\n", count);
  // delay(3000);
 
  if (Verbose)
  {
    Serial.printf("Search for firmware file '%s' ... ", FirmwareFilename);
  }

  int result = update_from_sdcard(FirmwareFilename, Verbose);
  if (result == ErrNoFile)
  {
    if (Verbose)
    {
      Serial.printf("not found, update not done\n");
    }
  }
  else if (result == ErrBadUpdate)
  {
    if (Verbose)
    {
      Serial.printf("file found but update failed\n");
      ESP.reset();
    }
  }
  else
  {
    if (Verbose)
    {
      Serial.printf("bad return code from update_from_sdcard(): %d\n", result);
      ESP.reset();
    }
  }
}
 
void loop(void)
{
  Serial.printf("loop: version %s\n", Version);
  delay(5000);
}