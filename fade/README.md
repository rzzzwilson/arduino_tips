# fade

An example sketch to show fading of an LED string from  one colour to
another at various time in the minute.  Could be easily changed to
change at various minutes in the hour or hours in the day.

Uses the *../schedule* library.
