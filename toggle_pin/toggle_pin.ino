/*
  Code to show how to "toggle" a pin.

  There are two LEDs on pins "led_red" and "led_green".
 */

// pins controlling the LEDs
const int led_red = 6;      // Iota D7, red
const int led_green = 8;    // Iota B4, green

void togglePin(int pin)
{
  digitalWrite(pin, !digitalRead(pin));
}

void setup(void)
{
  pinMode(led_red, OUTPUT);
  pinMode(led_green, OUTPUT);

  digitalWrite(led_green, HIGH);  // green on, red defaults to off
}

void loop(void)
{
  togglePin(led_red);
  togglePin(led_green);
  delay(1000);
}
