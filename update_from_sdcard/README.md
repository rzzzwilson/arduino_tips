# update_from_sdcard

A simple little program to show how to update firmware on the ESP8266
from a file on an SD card.

## Installation

Compile and load the code into the 8266 in the normal way.  The level of
verbosity is controlled by the *Verbose* value in the code.

To update the code from the SD card modify the code any way you want.  If you
don't want to make any particular change then just update the *Version*
constant.`

Create the *firmware.bin* file by selecting "Sketch|Export Compiled Binary"
and looking in the *build* subdirectory for the *\*.bin* file which you rename
to *firmware.bin*.  Format an SD card (<= 2GB) and place the *firmware.bin*
file on the card.

Start the ESP8266and monitor the serial output in the monitor.  What you will
see Depends on the *Verbose* setting in the code but you should always see
the output from the *loop()* code telling the version of the running code.
