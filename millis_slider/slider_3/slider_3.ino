#include <ezButton.h>
#include <Adafruit_NeoPixel.h>


// Attach the NeoPixel strip Din to the DataPin.
// Change NumLeds to the number of LEDs in the strip
const int DataPin = D2;
const int NumLeds = 8;
const int DisplayBrightness = 64; // display brightness

// define the pixels
Adafruit_NeoPixel pixels(NumLeds, DataPin, NEO_GRB + NEO_KHZ800);

// define the pin that has the button
const int ButtonPin = D3;
// setup the ezButton data
ezButton FireButton(ButtonPin);

// values for sliding
const int SlideInterval = 20;   // milliseconds between slide changes

// the timestamp when we last called millis()
// used to control when to call "slide()"
unsigned long prev_millis = 0;


//---------------------------------------------------------
// Do one step of a slide.
//
// This code is called at regular intervals by "loop()".
// Moves the NeoPixel data one pixel to the right.
// The pixel at index 0 is cleared.
//---------------------------------------------------------

void slide()
{
  // we do show() first because "fire" puts new pixel at index 0
  pixels.show(); 

  // then we slide LED data one pixel to the right
  // we loop over all but first pixel, but in reverse!
  for (int i = NumLeds-1; i > 0; --i)
  {
    pixels.setPixelColor(i, pixels.getPixelColor(i-1));    
  }
  
  // clear the new pixel at index 0
  pixels.setPixelColor(0, 0, 0, 0);
}

void setup()
{
  Serial.begin(115200);
  delay(4000);

  // initialize the NeoPixel strip
  pixels.begin();
  pixels.setBrightness(DisplayBrightness);  // set "static" brightness
  pixels.clear();

  Serial.println("Start");
}

void loop()
{
  unsigned long now = millis();
  
  // check the button
  FireButton.loop();

  // check if we have to call the slide() function
  if (now - prev_millis >= SlideInterval)
  {
    // call the display routine which may terminate the sliding
    slide();

    // set time for NEXT call of slide()
    prev_millis = now;
  }

  // check the button has been pressed and released
  if (FireButton.isReleased())
  {
    Serial.print("Button: ");
    Serial.println(now);

    // turn on pixel 0, slide() will do the rest
    pixels.setPixelColor(0, 255, 0, 0);
  }
}
