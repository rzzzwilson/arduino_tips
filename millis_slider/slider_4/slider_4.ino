#include <ezButton.h>
#include <Adafruit_NeoPixel.h>

// uncomment the #define below to turn on Serial debug
//#define DEBUG

// Attach the NeoPixel strip Din to the DataPin.
// Change NumLeds to the number of LEDs in the strip
const int DataPin = D2;
const int NumLeds = 8;

const int DisplayBrightness = 64; // display brightness

// define the pixels
Adafruit_NeoPixel pixels(NumLeds, DataPin, NEO_GRB + NEO_KHZ800);

// define the pin that has the button
const int ButtonPin = D3;
// setup the ezButton data
ezButton FireButton(ButtonPin);

// values for sliding
const int SlideInterval = 50;   // milliseconds between slide changes

// the timestamp when we last called millis()
// used to control when to call "slide()"
unsigned long prev_millis = 0;

// define the hidden buffer for a burst
const int NumInBurst = 5;
uint32_t hidden_burst[NumInBurst];

// define the colours in a "burst"
const uint32_t Burst[NumInBurst] = 
    {
      pixels.Color(50, 20, 20),   // the last pixel of the burst
      pixels.Color(100, 25, 25),
      pixels.Color(150, 75, 75),
      pixels.Color(230, 230, 255),
      pixels.Color(3, 3, 25)    // this value "overflows" to the strip
    };

//---------------------------------------------------------
// Slide the hidden buffer.
//
// Like the NeoPixel slide, but the "overflow" value copied
// to the first pixel of NeoPixel strip.
// The first pixel of the hidden buffer is cleared.
//---------------------------------------------------------

void slide_hidden()
{
  // copy overflow pixel to the real strip
  pixels.setPixelColor(0, hidden_burst[NumInBurst-1]);

  // then slide to the right
  for (int i = NumInBurst-1; i > 0; --i)
  {
    hidden_burst[i] = hidden_burst[i-1];
  }

  // and clear the first hidden pixel
  hidden_burst[0] = pixels.Color(0, 0, 0);
}

//---------------------------------------------------------
// Do one step of a slide.
//
// This code is called at regular intervals by "loop()".
// Moves the NeoPixel data one pixel to the right.
// The pixel at index 0 is copied from the idden buffer.
//---------------------------------------------------------

void slide()
{
  // we do show() first because "fire" puts new pixel at index 0
  pixels.show(); 

  // then we slide LED data one pixel to the right
  // we loop over all but first pixel, but in reverse!
  for (int i = NumLeds-1; i > 0; --i)
  {
    pixels.setPixelColor(i, pixels.getPixelColor(i-1));    
  }

  // move one pixel of the hidden buffer into the strip
  slide_hidden();
}

void setup()
{
#ifdef DEBUG
  Serial.begin(115200);
  delay(4000);
#endif

  // initialize the NeoPixel strip
  pixels.begin();
  pixels.setBrightness(DisplayBrightness);  // set "static" brightness
  pixels.clear();

#ifdef DEBUG
  Serial.println("Start");
#endif
}

void loop()
{
  unsigned long now = millis();
  
  // check the button
  FireButton.loop();

  // check if we have to call the slide() function
  if (now - prev_millis >= SlideInterval)
  {
    // call the display routine which may terminate the sliding
    slide();

    // set time for NEXT call of slide()
    prev_millis = now;
  }

  // check the button has been pressed and released
  if (FireButton.isReleased())
  {
#ifdef DEBUG
    Serial.print("Button: ");
    Serial.println(now);
#endif
    
    // fill the hidden buffer with the "burst" pixels
    // do one hidden slide to put first pixel of burst into strip
    // and "slide()" will do the rest.
    memmove(hidden_burst, Burst, sizeof(Burst));  // can be used instead of loop
//    for (int i = 0; i < NumInBurst; ++i)
//    {
//      hidden_burst[i] = Burst[i];
//    }

    slide_hidden();
  }
}
