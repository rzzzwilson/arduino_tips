# Millis slider

The code here came about due to a question on /r/arduino:

https://old.reddit.com/r/arduino/comments/10pwgev/how_do_i_replace_delay_while_maintaining_the_same/

## First version

The Arduino example sketch
[*Blink without delay()*](https://www.arduino.cc/en/Tutorial/BuiltInExamples/BlinkWithoutDelay)
blinks the LED continuously using a simple state variable that is the current
LED state.  The idea here (use *millis()*) is the basis for a solution to the
question above.

A simple continuous slide is relatively easy using the ideas in *Blink without
delay()*.  We need a slightly different state variable holding the currently
displayed LED in the strip.  So the time code would just call a function
`slide()` every whenever and that function would increment the state variable,
wrapping around to 0 if off the end of the strip, clear the strip display and
then turn on the required LED.  That's not too hard.

But having a button initiate one slide that terminates at the end of the strip
is a bit harder.  We need to add another state boolean variable *sliding* that
is *true* if a slide is occurring and *false* if there is no current slide
happening.  We also need to decide if the button does anything if a slide is
currently occurring.  It's simpler if nothing happens when the button is pressed
during a slide.  So in `loop()` we check if we are sliding or not.  If we are
sliding just check the time to see if we need to call the `slide()` function or
not.  If we are not sliding check the button to see if we want to start a slide.

In this approach we check the *sliding* state variable and if that is true we
call the slide function if the time is right.  If we aren't sliding we check
the fire button.  If that's down we start a slide.  Note that if sliding we 
don't check the button.

The code for this is in *slide_1/slide_1.ino*.

## Second version

That decision to ignore the button while a slide is occurring intrigued me.  I
knew that something could be done, but time was short so I let it go.  Then the
person who raised the original question asked a follow-up question:

> I noticed when I click on the button, the next LED animation won't start until the current one is completed. Is there any way to make it [start an "overlapped" slide?]

The answer is, like most things in the Arduino world, "Yes of course, but it's
more complicated.".

### Approach 1

There are two approaches that I can think of.  We will need more state variables
because we have more "state".

In the first approach we *could* have an array that has as many elements in it
as there are LEDs in the strip and each element would show the current state of
that LED in the strip.  The slide then just moves the elements of the array one
place to the right filling in position 0 with an "off" value.  Then change the
LED strip to match the state in the array.  This would work but it uses a lot of
memory, which is always in short supply on most Arduino boards.

So instead of having an entire array mirroring each LED in the strip we could
have an array containing only the indices of the "on" LEDs in the strip.  This
replaces the *slide_pixel* state variable.  A downside is that we need to decide
how big the array will be and now an enthusiastic player could press fire when
there are no empty slots left.  So it's possible a button push must be ignored.

The slide code would:

1. Iterate through through the array incrementing each index.  If the new index
  value is off the length of the LED strip that index is removed.
2. Clear the LED strip.
3. Turn on all LEDs with an index in the array.
4. Show the pixels.

That should work.

The *slide_2.ino* code no longer uses a simple *digitalRead()* to check the fire
button.  If we did that we would get many pushes per second.  This is because
there is no longer the concept of checking the button only if we are not sliding.
In this version of the code we are *always* sliding so we now need to use
a button library to handle the fire button.  We add another pixel to the
slide when we get a button *release* event.  Using a button library means
we also get debounce on the button, an added bonus.

The code is in *slide_2/slide_2.ino*.

### Approach 2

A more advanced approach uses the understanding that the NeoPixel library
uses an in-memory copy of the RGB value for each pixel in the strip.  It might
be possible to use that data, so we wouldn't need the *slide_pixel* array at
all.  Of course, fiddling with the data structures used by the NeoPixel library
is fraught with peril, but with care, and after looking at the library code
to see how the LED data is stored in memory, we might get something useful.

I initially thought of using the
[C library `memmove()` function](https://man7.org/linux/man-pages/man3/memmove.3.html)
which is the fastest way to move data around in memory, but I got crashes trying
to use that function.  Then I remembered that the NeoPixel library has the
[`getPixelColor()` function](https://adafruit.github.io/Adafruit_NeoPixel/html/class_adafruit___neo_pixel.html#abccaba27e35bfae27c856e76b46c7924)
to match the `setPixelColor()` function.  Using that in a loop to slide the
existing pixels worked well.  The code that recognizes a fire button release
would only need to set the index 0 LED to a fire colour using `setPixelColor()`.

This is the best solution, with minimum complexity and needing the least lines!

This solution is in *slide_3/slide_3.ino*.

### Enhancements

If you really want to *WOW* people you can make the effect more complex.
Because the actual pixel data is used to store pixel state we could use colour
without adding any more complexity to the code.  For example, if you wanted a
blue pixel you just use the blue colour.

More importantly, instead of using one pixel as the sliding element you could
use three, four or more.  This would allow for brightness and colour changes in
the LED burst to add more "theatre".  For instance, in the `loop()` code where
it is decided that a button has been pressed, instead of doing this boring 
one pixel display:

    // turn on pixel 0, slide() will notice
    pixels.setPixelColor(0, 255, 0, 0);

you could initiate a three pixel slide, with colour and intensity changes:

    // turn on pixels, slide() will notice
    pixels.setPixelColor(0, 32, 0, 64);
    pixels.setPixelColor(1, 255, 0, 0);
    pixels.setPixelColor(2, 32, 0, 64);

That's all you have to do because the *slide()* function just moves whatever
values are in the pixels.

A further enhancement that may not be worth the effort.  Notice that the three
pixel (or more) burst starts fully formed: all pixels appear at once.  It might
be nicer if you could make them appear as if they are sliding in from a hidden
buffer.  That is, in a three pixel burst you would show only the first pixel
of the burst at index 0.  On the next update you show the original pixel at 1
and the second pixel at index 0, and so on.  This isn't too hard if you keep
an array of "off display" pixels that's big enough to hold one burst.  So the
code that decides the button has been pressed would update the 0 index display
pixel with the first value of the burst and fill the hidden buffer with the rest
of the burst values.  Now *slide()* does what it normally does, but at index 0
instead of just clearing it as before, it moves in the next value from the
hidden buffer and slides the values in the buffer to the right just as it did
for the NeoPixel data.

The details are left as an exercise!
