#include <ezButton.h>
#include <Adafruit_NeoPixel.h>

// uncomment to turn on debug code
//#define DEBUG

// Attach a NeoPixel strip Din to the DataPin.
// Change NumLeds to the number of LEDs in the strip
const int DataPin = D2;
const int NumLeds = 8;
const int DisplayBrightness = 64; // display brightness

// define the pixels
Adafruit_NeoPixel pixels(NumLeds, DataPin, NEO_GRB + NEO_KHZ800);

// define the pin that has the button
const int ButtonPin = D3;
// setup the ezButton data
ezButton FireButton(ButtonPin);

// values and state variables for sliding
const int NumShots = 3;         // max number of concurrent shots
const int SlideInterval = 100;  // milliseconds between slide changes
bool sliding = false;           // "true" if we are sliding
int slide_pixels[NumShots];     // the displayed pixels during a slide
                                // this holds the NEXT index to show


// DEBUG routines, uncomment the DEBUG #define line at the top to turn this on
void debug_slide_pixels(const char *msg)
{
#ifdef DEBUG
  Serial.println(msg);
  Serial.println("slide(): slide_pixels=");
  for (int i = 0; i < NumShots; ++i)
  {
    Serial.print(slide_pixels[i]);
    Serial.print(", ");
  }
  Serial.println();
#endif
}

//---------------------------------------------------------
// Do one step of a slide.
//
// Note that this ALWAYS updates the strip.
//
// The indices in the array are now the "next to be displayed"
// indices and we increment the indices AFTER showing the strip.
// This is different from before because we want -1 in the array
// to indicate UNUSED.
//---------------------------------------------------------

void slide()
{
  debug_slide_pixels("slide");   // DEBUG
    
  // show the LED strip
  pixels.clear();
  for (int i = 0; i < NumShots; ++i)
  {
    if (slide_pixels[i] >= 0)
    {
      pixels.setPixelColor(slide_pixels[i], pixels.Color(255, 0, 0));
    }
  }
  pixels.show(); 

  // increment the pixel indices in "slider_pixels"
  for (int i = 0; i < NumShots; ++i)
  {
    if (slide_pixels[i] >= 0)    // only process "active" entries
    {
      slide_pixels[i] += 1;
      if (slide_pixels[i] > NumLeds)
      {
        // remove from consideration the index that is off the end
        slide_pixels[i] = -1;
      }
    }
  }
}

void setup()
{
  Serial.begin(115200);
  delay(4000);

  // initialize the NeoPixel strip
  pixels.begin();
  pixels.setBrightness(DisplayBrightness);  // set "static" brightness
  pixels.clear();

  // clear the "sliding_pixels" state array
  for (int i = 0; i < NumShots; ++i)
  {
    slide_pixels[i] = -1;   // -1 means "unused"
  }
  debug_slide_pixels("slide");   // DEBUG

  Serial.println("Start");
}

void loop()
{
  static unsigned long prev_millis = 0; // "static" means lasts between loop() calls
  unsigned long now = millis();
  
  // check the button
  FireButton.loop();

  // check if we have to call the slide() function
  if (now - prev_millis >= SlideInterval)
  {
    // call the display routine which may terminate the sliding
    slide();

    // set time for NEXT call of slide()
    prev_millis = now;
  }

  // check the button has been pressed and released
  // we check for relase so we don't get LOTS of pushes
  if (FireButton.isReleased())
  {
    Serial.print("Button: ");
    Serial.println(now);

    // add a new pixel index to the array, if there's room.
    // note that the array isn't sorted, so we have to look for a -1.
    // we need a flag to tell us after the loop that we couldn't find
    // an empty slot which is an error.
    bool found = false;
    for (int i = 0; i < NumShots; ++i)
    {
      if (slide_pixels[i] == -1)
      {
        slide_pixels[i] = 0;
        found = true;
        debug_slide_pixels("button pressed");
        break;    // found a slot, break out of the loop
      }
    }
    if (!found)   // if we didn't set "found" to true above
    {
      Serial.println("Too many shots! Ignored.");
    } 
  }
}
