//*****************************************************************
// Code to demonstrate reading from the Serial input, processing
// a result and sending it back through the Serial connection.
//
// This simple example:
//   * doesn't use the String object
//   * converts with the strtol() library function
//   * has error notification
//*****************************************************************

void setup()
{
  Serial.begin(115200);
  delay(2500);  // to ensure the following "READY" is seen

  // this helps us if the board crashes and restarts
  Serial.println("READY");
}

// allow at most 16 binary digits
const int MAX_DIGITS = 16;
char buffer[MAX_DIGITS + 2];
char *bad_ptr;

void loop()
{
  if (Serial.available() > 0)
  {
    int len = Serial.readBytesUntil('\n', buffer, MAX_DIGITS+1); 

    buffer[len] = '\0';
    unsigned long dec = strtol(buffer, &bad_ptr, 2);

    // decide if the whole buffer was converted
    if (*bad_ptr != '\0')
    {
      Serial.println("BAD");
    }
    else
    {
      // check if overflow, flush any unread characters
      delay(10);    // needed, see README
      
      if (Serial.available() > 0)
      {
        Serial.println("OVF");
        
        // flush unread characters
        while (Serial.read() != -1)
        {
          ;
        }
      }
      else
      {
        Serial.println(dec);
      }
    }
  }
}
