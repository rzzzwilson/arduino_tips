# advanced_without_delay

The excellent Arduino builtin code examples have many running examples with an
[explanation on the Arduino website](https://www.arduino.cc/en/Tutorial/BuiltInExamples).
One of the most important examples is the
[Blink Without Delay](https://www.arduino.cc/en/Tutorial/BuiltInExamples/BlinkWithoutDelay)
example.

When starting with Arduino beginners are shown the *delay()* function that is
used to delay the user's sketch a certain time.  Using *delay()* it's easy to
blink an LED, for example.  The *blink/blink.ino* file is a copy of the Arduino
example.

The schematic for *blink.ino* is ![blink.ino schematic](Schematic_LED.png)

It's easy to reason about your code if it uses *delay()*, but the moment you
start to want to do more than one thing at a time you run into problems.
Suppose we want a button that will change the LED blink period from 1/2 second
to 2 seconds and back to 1/2 second on a further push, and so.  An attempt to
do this using *delay()* has problems, as shown in the
 *blink_button/blink_button.ino* sketch.  When you run it you find that it's 
hard to get the button to change the period.  Most of the time a button push
doesn't do anything.  That's because most of the time the code is doing nothing
but executing the *delay()* function, so it can't read the button state.  Only
if the button is down when the LED state changes does anything happen.

The answer is to not use *delay()* but use the approach used in the "blink
without delay" example.  That is, we "schedule" an execution of a function
for a time in the future and the *loop()* code checks the time and calls the 
function when the time is right.  That means that the *loop()* code can also
check the button state every time it is called.  If the button is pressed
then the code changes the delay period in *blink_period* and the next time
the *blink()* function is called is uses the updated period.  The code showing
this is in *blink_button_millis/blink_button_millis.ino*.

The schematic for *blink_button.ino* and *blink_button_millis.ino* is
![LED plus button](Schematic_LED_Button.png)

When running *blink_button_millis* you find that sometimes the *Serial Monitor*
shows more than one change for a button press.  This is "bounce" in the button.
Buttons aren't perfect and must be debounced.  There's another Arduino example
showing how to do that.
