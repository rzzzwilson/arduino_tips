//*****************************************************************
// Code to demonstrate reading from the Serial input, processing
// a result and sending it back through the Serial connection.
//
// This simple example:
//   * doesn't use the String object
//   * converts with its own code
//   * has error notification
//*****************************************************************

void setup()
{
  Serial.begin(115200);
  delay(2500);  // to ensure the following "READY" is seen

  // this helps us if the board crashes and restarts
  Serial.println("READY");
}

// allow at most 16 binary digits
const int MAX_DIGITS = 16;
char buffer[MAX_DIGITS + 2];
bool bad_flag = false;

void loop()
{
  if (Serial.available() > 0)
  {
    bad_flag = false;
    int len = Serial.readBytesUntil('\n', buffer, MAX_DIGITS+1); 

    buffer[len] = '\0';

    // convert buffer to a decimal value
    unsigned int dec = 0;
    for (int i = 0; i < len; ++i)
    {
      int value = buffer[i] - '0';

      if (value != 0 && value != 1)
      {
        bad_flag = true;
        break;
      }
      
      dec = (dec << 1) + value;
    }

    // check for a BAD character
    if (bad_flag)
    {
      Serial.println("BAD");
    }
    else
    {
      // check if overflow, flush any unread characters
      delay(10);    // needed, see README
      
      if (Serial.available() > 0)
      {
        Serial.println("OVF");
        
        // flush unread characters
        while (Serial.read() != -1)
        {
          ;
        }
      }
      else
      {
        Serial.println(dec);
      }
    }
  }
}
