//#define DEBUG

#ifdef DEBUG
  #define dprint(...)     Serial.print(__VA_ARGS__)
  #define dprintln(...)   Serial.println(__VA_ARGS__)
#else
  #define dprint(...)   
  #define dprintln(...) 
#endif

void setup()
{
#ifdef DEBUG
  Serial.begin(115200);
  delay(2500);
#endif

  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
  dprint(millis(), HEX);
  dprintln(": DEBUG is on");

  // double flash LED every second or so
  digitalWrite(LED_BUILTIN, HIGH);
  delay(100);
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(100);
  digitalWrite(LED_BUILTIN, LOW);
  delay(700);
}
