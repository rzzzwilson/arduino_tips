# Serial processing

Something that arduino users might want to do is to use the Serial
monitor to send commands to the arduino board, do something with
it, and return the result to the PC through the Serial monitor.

The code here shows how.  The example used is from /r/arduino and
allows the conversion of a binary string like "100110" to decimal
on the arduino board.  The result is sent back to the PC.

## Version 1 - simple

The smallest, simplest version we can write uses the methods of
the Serial object to read data into a String object.  The conversion
to a decimal number is performed by the `strtol()` library function.
The `loop()` function is:

    void loop()
    {
      if (Serial.available() > 0)
      {
        String message = Serial.readStringUntil('\n'); 
        unsigned long dec = strtol(message.c_str(), NULL, 2);
        Serial.println(dec);
      }
    }

We don't want to hang on the Serial input waiting for input and timing
out if there is none, so we check if there is Serial data available
before trying to read a \\n terminated string from Serial.  If there is
input we read the input into a `String` and convert it with the `strtol()`
library function.  This function only works on C-style strings, so the
code call the `.c_str()` method of the `String` object.

This approach handles any number of binary digits the user types in, but
has a few shortcomings:

* Use of the `String` object is not recommended for microcontrollers
  with limited memory
* There is no error checking

The code for this version is in the `example1` directory.

## Version 2 - don't use String

The `String` object dynamically allocates and frees memory from the heap
and on a limited memory microcontroller this can lead to memory fragmentation
and crashes.  We still need somewhere to assemble a message we receive 
from the Serial connection, so we have to allocate a fixed length buffer
for the message.  However, buffers can overflow, but we will ignore
that for now and worry about that when we are handling errors.

This initial code is similar to `example1`:

    // allow at most 16 binary digits
    const int MAX_DIGITS = 16;
    char buffer[MAX_DIGITS + 2];
    
    void loop()
    {
      if (Serial.available() > 0)
      {
        int len = Serial.readBytesUntil('\n', buffer, MAX_DIGITS); 
    
        buffer[len] = '\0';
        unsigned long dec = strtol(buffer, NULL, 2);
        Serial.println(dec);
      }
    }

We use `readBytesUntil()` because that fills a buffer and doesn't return a
`String` object.  The filled buffer isn't a C-style string because it lacks
the NULL terminator, so we have to add it using the length value returned
by `readBytesUntil()`.  After that we proceed as before.

Testing works fine, up to 15 digits, but if we type in 16 or more digits
we see something odd.

    READY
        > 111111111111111		# 15 digits
    32767
        > 1111111111111111		# 16 digits
    65535
    0
        > 11111111111111111		# 17 digits
    65535
    1

What is happening is due to the behaviour of the `readBytesUntil()` method.
If we tell the function the buffer is 16 characters long it won't fill the
buffer past the end, that would be disasterous.  What it does is it stops
reading characters from `Serial` once it has read 16 characters and returns.
Those remaining characters after the first 16 are read on the next execution
of the `loop()` function.  To solve this we have to flush any unread
characters from `Serial` after the `readBytesUntil()` method returns:

    // allow at most 16 binary digits
    const int MAX_DIGITS = 16;
    char buffer[MAX_DIGITS + 2];
    
    void loop()
    {
      if (Serial.available() > 0)
      {
        int len = Serial.readBytesUntil('\n', buffer, MAX_DIGITS); 
    
        buffer[len] = '\0';
        unsigned long dec = strtol(buffer, NULL, 2);
        Serial.println(dec);
    
        // flush unread characters
        delay(10);    // needed, see README
        
        while (Serial.read() != -1)
        {
          ;
        }
      }
    }

This code is in the `example2` directory.

Note: the `delay()` used when flushing unread characters is from
https://forum.arduino.cc/t/solved-flush-serial-input-buffer/525657 .

# Version 3 - add error notification

There are a few errors that can occur in the previous code:

* buffer overflow
* bad input, containing characters other than "0" or "1"

The code in example2 will silently handle these errors, but we would
like the user to get some indication of these errors because if the user
types "123" the previous code will just echo "1" without any indication
of a problem.  We want the code to send these error strings back instead
of a number:

* OVF - the input overflowed the input buffer
* BAD - a bad character was found in the user string

The overflow error is easily handled.  We know overflow occurred if the
code is flushing unread characters, so we just send "OVF" back instead 
of the number we converted.

Bad characters is a little more complicated.  We can use extra features
of the `strtol()` function to do this.  The function will convert all
characters that it can for the given base and will stop on the first
unacceptable (ie, BAD) character, returning a pointer to the unconverted
character.  If the entire buffer was converted that pointer will point
to the NULL byte terminating the buffer.

So the final code is:

    // allow at most 16 binary digits
    const int MAX_DIGITS = 16;
    char buffer[MAX_DIGITS + 2];
    char *bad_ptr;
    
    void loop()
    {
      if (Serial.available() > 0)
      {
        int len = Serial.readBytesUntil('\n', buffer, MAX_DIGITS+1); 
    
        buffer[len] = '\0';
        unsigned long dec = strtol(buffer, &bad_ptr, 2);
    
        // decide if the whole buffer was converted
        if (*bad_ptr != '\0')
        {
          Serial.println("BAD");
        }
        else
        {
          // check if overflow, flush any unread characters
          delay(10);    // needed, see README
          
          if (Serial.available() > 0)
          {
            Serial.println("OVF");
            
            // flush unread characters
            while (Serial.read() != -1)
            {
              ;
            }
          }
          else
          {
            Serial.println(dec);
          }
        }
      }
    }

This code is in the `example3` directory.

# Addendum

Using the `strtol()` function does add some code size to the program.  If you
**really** want to save code size you don't use the library function but convert 
the characters in the buffer yourself:

    int dec = 0;
    for (int i = 0; i < strlen(buffer); i++)
    {
      dec = (dec << 1) + buffer[i] - '0';
    }

This may save some space, though the simple code above needs to handle
"BAD" characters in such a way that we can notify the user.

This additional code is in the `example4` directory.  The code saving over
`example3` code is roughly 750 bytes.
