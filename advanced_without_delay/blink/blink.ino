// define the LED stuff, pin and state
const int LedPin = 13;                        // where the LED is connected
const int LedPeriod = 500;                    // 1/2 second on, 1/2 second off
int led_state = LOW;                          // initial state of LED pin


void blink()
{
  // first, flip the LED state
  if (led_state == LOW)
  {
    led_state = HIGH;
  }
  else
  {
    led_state = LOW;
  }

  // then write new value to the pin and delay
  digitalWrite(LedPin, led_state);
  delay(LedPeriod);
}

void setup()
{
  Serial.begin(115200);
  
  // set the LED pin mode and set initial state
  pinMode(LedPin, OUTPUT);
  digitalWrite(LedPin, led_state);
}

void loop()
{
  // then blink the LED at the saved period
  blink();  // one cycle of blink, turn ON or OFF
}
