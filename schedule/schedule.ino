/*
 * Code to exercise the "schedule" module.
 *
 * There are three LEDs on pins "led_blue", "led_red" and "led_green".
 * Turn them on and off at different times without using arduino delay().
 * 
 * There is also a button between the "btn" pin and ground. Pressing the
 * button restarts the LED display.
 */

#include "schedule.h"

// pins controlling three LEDs
const int led_blue = 12;
const int led_red = 8;
const int led_green = 6;

const int btn = 9;

// periods for each LED
const int blue_period = 97;     // three primes,
const int red_period = 487;     //   *almost* multiples of the previous value
const int green_period = 1459;

void event_abort(void);             // forward definition

Schedule schedule(3, event_abort);  // initialize 3 slots plus our full abort handler

bool abort_flag = false;            // true if the code should abort


//======================================================================
// Function called if there is a problem in the "schedule" code.
//======================================================================

void event_abort(void)
{
#ifdef SCHED_DEBUG
  Serial.println("ABORT: event_abort() called!");
#endif

  abort_flag = true;
}


//======================================================================
// Function that toggles a digital pin.
//======================================================================

void toggle_pin(int pin)
{
  // read the pin and toggle its state
  digitalWrite(pin, !digitalRead(pin));
}


//======================================================================
// LED toggle handlers.
//
// There are three LEDs on pins "led_blue", "led_red" and "led_green".
// Turn them on and off at different times.
//======================================================================

void blue_toggle(void)
{
  toggle_pin(led_blue);
  schedule.schedule(blue_period, blue_toggle);
}

void red_toggle(void)
{
  toggle_pin(led_red);
  schedule.schedule(red_period, red_toggle);
}

void green_toggle(void)
{
  toggle_pin(led_green);
  schedule.schedule(green_period, green_toggle);
}


//======================================================================
// Start the LEDs.  Flash them first.
//======================================================================

void start_led(void)
{
  // exercise all LEDs as a signon test
  digitalWrite(led_blue, LOW);
  digitalWrite(led_red, LOW);
  digitalWrite(led_green, LOW);
  delay(500);
  digitalWrite(led_blue, HIGH);
  digitalWrite(led_red, HIGH);
  digitalWrite(led_green, HIGH);
  delay(500);
  digitalWrite(led_blue, LOW);
  digitalWrite(led_red, LOW);
  digitalWrite(led_green, LOW);
  delay(500);

  // start all LED colour cycles
  schedule.schedule(red_period, red_toggle);
  schedule.schedule(green_period, green_toggle);
  schedule.schedule(blue_period, blue_toggle);
}


//======================================================================
// Setup the system.
//======================================================================

void setup(void)
{
#ifdef SCHED_DEBUG
  // initialize the Serial connection, if required
  Serial.begin(115200);
  delay(2500);    // wait for Serial
#endif

  // initialize the LED pins as OUTPUT
  pinMode(led_blue, OUTPUT);
  pinMode(led_red, OUTPUT);
  pinMode(led_green, OUTPUT);

  // button pin is INPUT
  pinMode(btn, INPUT_PULLUP);

  // start the LEDs
  start_led();
}


//======================================================================
// Loop code.
//======================================================================

void loop(void)
{
  if (abort_flag) // if we are aborted, do nothing
    return;       // have to keep loop()ing on an Arduino

  if (digitalRead(btn) == LOW)
  {
#ifdef SCHED_DEBUG
    Serial.println("Button pushed!");
#endif

    // flush the events table, restart the flashing
    schedule.flush();
    start_led();  // simple, just start LEDs again
  }

  // show we aren't waiting on delay() anywhere
  // this shows we get 4 to 6 loop() calls per millisecond
#ifdef SCHED_DEBUG
  Serial.println(millis());
#endif
  
  schedule.tick();   // kick the schedule system along
}
