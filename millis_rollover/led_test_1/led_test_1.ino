/////////////////////////////////////////////////////////////////////////
// Code to test millis() rollover handling code.
//
// There is an LED and button.  Pushing the button "latches" the LED on
// for a period.  Pushing the button while the latch LED is on should
// just extend the LED latch time.
//
// There is also a "warning" LED that turns on a fixed time before the 
// overflow occurs.
// 
// The short_millis() function behaves like the Arduino millis() function,
// but overflows about every 65.6 seconds.  It returns (unsigned int).
//
// This simplistic solution doesn't work.  Pressing the button while the
// warning LED is on just lights the LED while the button is down.
// Releasing the button turns the LED off.
/////////////////////////////////////////////////////////////////////////

// pin definitions
const int LatchPin = 12;      // pin for the internal LED, Iota = D6
const int SwitchPin = 10;     // pin for the switch,       Iota = B6
const int WarnPin = 13;       // pin for the warning LED,  Iota = C7

// time for "latch" LED hold
const int LatchTime = 20000;  // 20 seconds

// milliseconds before overflow to turn the warning LED on
const unsigned int WarnPeriod = 10000;  // 10 seconds

// time when latched LED is to be turned off
unsigned int unlatch_time = 0;   // next latch LED off time


/////////////////////////////////////////////////////////////////////////
// Like millis() but overflows after 0xffff milliseconds.
/////////////////////////////////////////////////////////////////////////

// time in milliseconds before short_millis() result overflows
const unsigned long max_millis = 0xffff;    // roughly 65.6s

unsigned int short_millis(void)
{
  return (unsigned int) (millis() & max_millis);  // overflow at 65.6s or so
}


void setup()
{
  // define latch LED pin as output
  pinMode(LatchPin, OUTPUT);

  // define warning LED pin as output
  pinMode(WarnPin, OUTPUT);

  // the SwitchPin must be INPUT_PULLUP
  pinMode(SwitchPin, INPUT_PULLUP);

  // indicate that we are ready, just flash both LEDs
  for (int i = 0; i < 4; ++i)
  {
    digitalWrite(LatchPin, HIGH);
    digitalWrite(WarnPin, LOW);
    delay(100);
    digitalWrite(LatchPin, LOW);
    digitalWrite(WarnPin, HIGH);
    delay(100);
  }

  // leave both LEDs off when finished
  digitalWrite(LatchPin, LOW);
  digitalWrite(WarnPin, LOW);
}

void loop()
{
  // get current time using our own function
  unsigned int now = short_millis();

  // if close to rollover, turn on warning LED
  digitalWrite(WarnPin, LOW);
  if (now > (max_millis - WarnPeriod))
  {
     digitalWrite(WarnPin, HIGH);
  }

  Serial.print("now=");
  Serial.print(now);
  Serial.print(", unlatch_time=");
  Serial.println(unlatch_time);
  delay(100);
  
  // if the latch time has been exceeded, turn off latch LED
  if (now > unlatch_time)
  {
    digitalWrite(LatchPin, LOW);
  }
  
  // if the switch closed, it will read LOW
  if (digitalRead(SwitchPin) == LOW)
  {
    unlatch_time = now + LatchTime;    // set "off" time in the future
    digitalWrite(LatchPin, HIGH);   // turn on "latch" LED
  }
}
