# Arduino template file

The code here is just to keep in one place all the code tricks
that might be used in an Arduino sketch.

## dprintf

A simple *dprintf()* function.  While convenient, it costs an extra
1.5K of flash and about 10 bytes of RAM.  Doesn't handle floats.

## Various tricks

The template also shows other little tricks, such as using readDigital()
to toggle an OUTPUT pin.
