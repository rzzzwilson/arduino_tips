/*
 * Schedule interface
 *
 * Library to handle asynchronous "events" in code without using delay().
 *
 * If SCHED_DEBUG is defined before including this file, some debug
 * routines are compiled in and usable.
 */

#ifndef __SCHEDULE_H__
#define __SCHEDULE_H__

#include "Arduino.h"

// if defined, include DEBUG code
//#define SCHED_DEBUG

// typedef for user function to handle an event
typedef void (*event_handler)(void);

// definition of an Event struct
typedef struct { 
                 unsigned long start;     // time event was scheduled
                 unsigned long period;    // time after "start" when triggered
                 event_handler handler;   // function executed when event triggers
               } Event;

class Schedule
{
  public:
    Schedule(int num_events, event_handler abort_handler=NULL);
    ~Schedule();
    void flush(void);
    void schedule(unsigned long period, event_handler handler);
    void tick(void);
    void on_idle(event_handler handler);
  private:
    Event *EventArray;                  // array of "Event"s
    int NextSlot;                       // index of first free Event slot in "EventArray"
    int NumEvents;                      // number of Event slots in the EventArray array
    event_handler AbortHandler = NULL;  // address of user "abort" function
    void abort(const char *);

#ifdef SCHED_DEBUG
    void dump(const char *msg);         // debug dump to Serial
#endif
};

#endif
