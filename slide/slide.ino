#include <SPI.h>
#include <Adafruit_NeoPixel.h>
#include "schedule.h"

///////////////////////////////////////////////////////
// Test code to slide an LED from position 0 to the end
// of the LED strip, repeat.
//
// Uses a "schedule" approach, code in files schedule.*.
///////////////////////////////////////////////////////

// Attach a NeoPixel strip Din to the DataPin.
// Change NumLeds to the number of LEDs in the strip
const int DataPin = D6;
const int NumLeds = 64;
const int DisplayBrightness = 16; // display brightness

// define the pixels
Adafruit_NeoPixel pixels(NumLeds, DataPin, NEO_GRB + NEO_KHZ800);

// schedule data
Schedule schedule(NULL, 2);

// how long between LEDs
const long SlideInterval = 250;

// index of the next LED to light
unsigned int NextLED = 0;

//*********************************************************
// Slide the LED.
//
// Restart at 0 if past end of strip.
//*********************************************************

void slide_led(void)
{
  // set the next LED
  pixels.clear();
  pixels.setPixelColor(NextLED, pixels.Color(255, 0, 0));
  pixels.show();
  
  // move to the next LED, wrap around if necessary
  if (++NextLED >= NumLeds)
  {
    NextLED = 0;
  }
  
  schedule.schedule(SlideInterval, slide_led);
  Serial.println(NextLED);
}

//*********************************************************
// Setup
//*********************************************************

void setup()
{
  pixels.begin();
  pixels.setBrightness(DisplayBrightness);  // set "static" brightness
  pixels.clear();
  pixels.show();
  
  Serial.begin(115200);

  // initialize the NeoPixel strip
  pixels.begin();
  pixels.setBrightness(DisplayBrightness);  // set "static" brightness
  pixels.clear();
  pixels.show();

  // start the slide
  schedule.schedule(SlideInterval, slide_led);

  delay(4000);
  Serial.println("Start");
}

//*********************************************************
// Call schedule.tick() to process active handlers.
//*********************************************************

void loop()
{
  // perform any scheduled activity
  schedule.tick();
}
