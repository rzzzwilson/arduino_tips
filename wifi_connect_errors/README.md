# What is this?

The sample code here shows how to report "bad SSID" and "bad password"
errors when connecting an ESP8266 controller to a WiFi access point.

# Why do we need this?

There is a status code to report that the given SSID doesn't exist:

    WL_NO_SSID_AVAIL

and this is returned by the WiFI libraries.

Despite there being WiFi status codes that flag a "bad password"
situation, like:

    WL_WRONG_PASSWORD
    STATION_WRONG_PASSWORD

the current libraries never return either of those statuses.  This
makes it hard to report meaningful errors back to the user.

## Workaround

The test code here has a configurable wait time for a good connect.
If that time is exceeded then a "bad password" is the assumed state.
Test with good SSID/password data, then a bad SSID and finally a
bad password.

The code also tries to nicely show the connection process, including
the common "connecting ..." display.

There appears to have been some discussion in the ESP8266 community
about why the WL_WRONG_PASSWORD status is never used, but the current
situation appears to be that this will never be fixed.
