#include <Adafruit_NeoPixel.h>

// Attach a NeoPixel strip Din to the DataPin.
// Change NumLeds to the number of LEDs in the strip
const int DataPin = D2;
const int NumLeds = 8;
const int DisplayBrightness = 64; // display brightness

// define the pixels
Adafruit_NeoPixel pixels(NumLeds, DataPin, NEO_GRB + NEO_KHZ800);

// define the pin that has the button
const int ButtonPin = D3;

// values and state variables for sliding
const int SlideInterval = 100;  // milliseconds between slide changes
bool sliding = false;           // "true" if we are sliding
int slide_pixel = -1;           // the displayed pixel index during a slide

//---------------------------------------------------------
// Do one step of a slide.
// If slide is finished sets "sliding" to false.
//---------------------------------------------------------

void slide()
{
  // increment the pixel index, check if finished
  slide_pixel += 1;
  if (slide_pixel >= NumLeds)
  {
    // slide is finished, clear display and set state to NOT SLIDING
    pixels.clear();
    pixels.show(); 
    sliding = false;
  }
  else
  {
    // show the pixel
    pixels.clear();
    pixels.setPixelColor(slide_pixel, pixels.Color(255, 0, 0));
    pixels.show(); 
  }
}

void setup()
{
  Serial.begin(115200);
  delay(4000);

  // initialize the NeoPixel strip
  pixels.begin();
  pixels.setBrightness(DisplayBrightness);  // set "static" brightness

  Serial.println("Start");
}

void loop()
{
  static unsigned long prev_millis = 0; // "static" means lasts between loop() calls
  unsigned long now = millis();

  // if we are sliding
  if (sliding)
  {
    // check if we have to call the slide() function
    if (now - prev_millis >= SlideInterval)
    {
      // call the display routine which may terminate the sliding
      slide();

      // set time for NEXT call of slide()
      prev_millis = now;
    }
  }
  else    // not sliding
  {
    // check the button
    if (digitalRead(ButtonPin) == 0)
    {
      Serial.print("Button: ");
      Serial.println(now);

      // start the sliding process, continued in the timed calls above
      sliding = true;
      slide_pixel = -1;  // -1 because slide() increments the index before displaying
      // the timed code above will start calling slide()
    }
  }
}
