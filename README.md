# arduino_tips

Place to store small pieces of code or ideas for the Arduino platform.

| directory | comments |
|-----------|----------|
| millis_rollover | Some code showing how to handle and test the `millis()` rollover |
| schedule  | An example of how to generalize the "blink without delay" arduino example |
| toggle_pin| Shows how to toggle a pin state without a state variable |
| update_from_sdcard | Shows how to update ESP8266 firmware from an SD card |
| sdcard    | Read/write a file on an microSD card |
