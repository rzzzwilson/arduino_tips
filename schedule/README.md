# schedule

A beginning arduino programmer often first tries the
[Blink sketch](https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink).
Soon after that they are told that using `delay()` isn't the best way to 
use the microcontroller and they are told about the
[Blink without delay()](https://www.arduino.cc/en/Tutorial/BuiltInExamples/BlinkWithoutDelay)
sketch, which uses the `millis()` clock value and allows other things to be
done while waiting.

The `schedule` library is a generalization of the 
[Blink without delay()](https://www.arduino.cc/en/Tutorial/BuiltInExamples/BlinkWithoutDelay)
approach which allows a configurable number of events to be handled.

## Design

The [Blink without delay()](https://www.arduino.cc/en/Tutorial/BuiltInExamples/BlinkWithoutDelay)
sketch creates an `unsigned long` variable that contains the time when
something should happen.  That simple example has a single LED to control so
the code in `loop()` knows what to do when the time is reached.  However, there
could be many more than one thing to be controlled with varying times for each.

We generalize the idea by creating what we call an "event".  This is the
combination of the time when something should happen and the function that
is to be called at that time.  We create a typedef and struct:

    // typedef for user function to handle an event
    typedef void (*event_handler)(void);

    // definition of an Event struct
    typedef struct { unsigned long start;     // time the event was scheduled
                     unsigned long period;    // time after "start" when the event is triggered
                     event_handler handler;   // function executed when the event triggers
                   } Event;

The function that will be called has the shown signature: a function that takes
no parameters and returns nothing.

So we will have one or more instances of the `Event` struct in memory and the
`schedule` code will need to periodically check all the existing `Event`
structs and compare the current time against the `when` time in each struct.

We keep an array of `Event` structs.  The array is not maintained in a sorted state,
the events are in any order.  This could be changed to keep events in order such 
that the next event to be actioned is at index 0, but that may not be worth the 
trouble for a small number of events.

The user will arrange for the periodic calling of a function that checks the
current time against the scheduled time of each event.  The function will call
the `handler` function associated with any expired event and *remove the
`Event` from the array*.

If some `Event` is supposed to happen periodically it is up to the `handler`
code to reschedule any required repetitions.

## Simple example

A small piece of arduino code `test/test.ino`:

    #include "schedule.h"
    
    Schedule schedule(2);	// 2 slots, no handler
    
    // the "scheduled" function
    void do_print(void)
    {
      Serial.print("Tick: ");
      Serial.println(millis());
      
      schedule.schedule(1500, do_print);  // reschedule the handler
    }
    
    void setup()
    {
      Serial.begin(115200);
      schedule.schedule(1500, do_print);
    }
    
    void loop()
    {
      schedule.tick();                 // check for handlers that should run
    }

When executed on an Arduino Uno we see this in the Serial Monitor:

    Tick: 3000
    Tick: 4500
    Tick: 6000
    Tick: 7500
    Tick: 9000
    Tick: 10500
    Tick: 12000
    Tick: 13500
    Tick: 15000
    Tick: 16500
    ...

## Larger example

The `schedule.ino` file is an Arduino sketch that controls three LEDs.  The
LEDs blink with varying periods.  The sketch code documents what pins are used
for the LEDs.  There is also a button that restarts the LED cycling when pressed.

This sort of thing is nearly impossible to do using `delay()`.  The `schedule`
code makes adding extra events very easy.

## API

**Schedule(int num_events, event_handler abort_handler=NULL);**

The *Schedule* constructor must be called first to initialize the system.

The *num_events* parameter is the number of event slots created in an internal
table.  If this table fills up an abort occurs.

The *abort_handler* parameter is the address of a user function taking no
parameters and returning nothing that does whatever needs to be done on a
**schedule** abort.  The abort condition occurs when user code tries to schedule
too many events for the internal table.  If the *abort_handler* is NULL then no
action is taken, the code will just busy idle.

--------

**void schedule(unsigned long period, event_handler handler);**

The *schedule* method is used by user code to schedule an event for the
future.

The *period* parameter is the number of milliseconds in the future the event is
triggered.

The *handler* parameter is the address of a user function that takes no
parameters and returns nothing.  This function is called when the event is
triggered.

---------

**void tick(void);**

The *tick* method should be called regularly by user code.  The event
table is scanned and pending events that are past their trigger time are
executed.

----------

**void flush(void);**

The *flush* method removes all scheduled events from the system.


----------

**void on_idle(event_handler handler);**

Sets the function `handler` to be called if the system ever goes "idle",
that is, there are no scheduled events in the schedule table.


## millis() rollover

The unsigned long integer returned by the *millis()* function increases for more
than 49 days before "rolling over" to 0 and starting to increase again.

An attempt has been made to make the *schedule* code handle any problems the
rollover might produce, but this has not been fully tested.
