# slide

An example sketch to show sliding of an LED in a string from position 0, to
1, 2, 3, etc.

Uses the *../schedule* library.
