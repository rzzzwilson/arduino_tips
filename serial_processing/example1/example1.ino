//*****************************************************************
// Code to demonstrate reading from the Serial input, processing
// a result and sending it back through the Serial connection.
//
// This simple example:
//   * uses the String object
//   * converts with the strtol() library function
//   * had no error checking
//*****************************************************************

void setup()
{
  Serial.begin(115200);
  delay(2500);	// to ensure the following "READY" is seen

  // this helps us if the board crashes and restarts
  Serial.println("READY");
}

void loop()
{
  if (Serial.available() > 0)
  {
    String message = Serial.readStringUntil('\n'); 
    unsigned long dec = strtol(message.c_str(), NULL, 2);
    Serial.println(dec);
  }
}
