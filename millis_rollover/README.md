# millis() Rollover

Anyone who has been programming on the Arduino platform for more than
a short time uses the `millis()` function to schedule operations instead
of using `delay()` to wait for a period.

Unfortunately, there is a problem using `millis()` for more than just a short
time.  The [millis() documentation](https://www.arduino.cc/reference/en/language/functions/time/millis/)
has this to say about the function::

    millis()

    Returns the number of milliseconds passed since the Arduino board began running the current program.
    This number will overflow (go back to zero), after approximately 50 days.

The usual way to handle a delay is to take the current value of `millis()`,
add the desired delay in milliseconds to that value and wait until that
time value is returned by `millis()`.  Unfortunately, if you sample the time
5 seconds before the rollover and add 10 seconds to that time value, you
will never see that "plus 10 seconds" time returned by `millis()` as the
returned time rolls over before the 10 seconds elapses.  Alternatively,
if you can handle the addition result including the 32 bit rollover the code
sees a much smaller time at which to turn off the LED and therefore switches it
off immediately, so the LED only appears on while the button is pressed.

Note that the solution discussed here only applies to relatively short
(compared to the rollover time) delays that might "straddle" the rollover.
Managing long delays, much longer than the rollover period, are possible but
this is not discussed here.

## The Solution

There are ways around the 49 day `millis()` rollover.
[This post from StackExchange](https://arduino.stackexchange.com/questions/12587/how-can-i-handle-the-millis-rollover)
discusses the problem and shows how to get around it.  The post is complex
but, put simply, you create a "duration" instead of a time to wait for, and
use the natural operation of a 32 bit unsigned value.

The link above shows the solution, but the more experienced among us will
naturally ask the question "how do I test this in my code?".  You could
write code that tests the 49 day rollover of the `millis()` function,
but that takes quite a while to accomplish and you should try something a
little shorter first before moving to the 49 day code.

One solution is to use your own `short_millis()` function that gets the
actual `millis()` value masked to only the N rightmost bits.  This will
overflow at much shorter periods than the `unsigned long` results.  In
addition, we need to realize that the N value to use must be 16 bits.
This allows the natural operation of a 16 bit unsigned value, which the
rollover solution depends on, to be duplicated in an `unsigned int`
return value.  So the `short_millis()` function looks like this::

    /////////////////////////////////////////////////////////////////////////
    // Like millis() but overflows after 0xffff milliseconds.
    /////////////////////////////////////////////////////////////////////////
    
    // time in milliseconds before short_millis() result overflows
    const unsigned long max_millis = 0xffff;    // roughly 65.6s
    
    unsigned int short_millis(void)
    {
      return (unsigned int) (millis() & max_millis);  // overflow at 65.6s or so
    }

This function will overflow after about 65.6 seconds, which is usable, even if
longer than we would like.

## Test Code

The setup that prompted this note has an LED and switch connected to an
Arduino.  When the button is pressed the LED turns on for a certain number
of seconds and then off.  If the button is again pressed while the LED is on
the LED stays on for the normal number of "latched" seconds after the second
push.

This is hard to do using `delay()`.  The simple solution using `short_millis()`
is (code skeleton):

    unsigned int unlatch_time = 0;          // next latch LED off time

    void loop()
    {
      unsigned int now = short_millis();    // current time
    
      // if the latch time has been exceeded, turn off latch LED
      if (now > unlatch_time)
      {
        digitalWrite(LatchPin, LOW);
      }
      
      // if the switch closed, it will read LOW
      if (digitalRead(SwitchPin) == LOW)
      {
        unlatch_time = now + LatchTime;     // set "off" time in the future
        digitalWrite(LatchPin, HIGH);       // turn on "latch" LED
      }
    }

This seems to work.  But it will fail if the button is pressed at a time
less than `LatchTime` seconds before the `millis()` rollover.  The LED will just
stay on while the button is pressed and won't latch.  This is because when we
add the latch period to the current time we get a small millisecond value less
than the current time and the code immediately turns the LED off again.

[The code is here](https://gitlab.com/rzzzwilson/arduino_tips/-/blob/master/millis_rollover/led_test_1/led_test_1.ino)

## Solution Code

We use the idea from the StackExchange link above.  When the button is pressed
we store the `short_millis()` time plus the LED latch time.  If the numeric
result is greater than 16 bits we get a time **less** than the current time,
that is, the `short_millis()` time 20 seconds in the future (the latch time).
This means our simple test in the previous code::

      if (now >= unlatch_time)

won't work properly - we just immediately turn the LED off again because the
unlatch time is less than the current time.  So we have to use the test
suggested by the link above, changed slightly because our time wraps around
at the 16 bit rollover::

    if ((unsigned int) (unlatch_time - now) > LatchTime)

Doing this makes the test code work correctly.  If we press the button while
the warning LED is on, we still turn the LED off at the correct time - button
press time plus `LatchTime`.

[The code for the correct version is here](https://gitlab.com/rzzzwilson/arduino_tips/-/blob/master/millis_rollover/led_test_2/led_test_2.ino)
