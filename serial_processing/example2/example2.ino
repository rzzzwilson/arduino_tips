//*****************************************************************
// Code to demonstrate reading from the Serial input, processing
// a result and sending it back through the Serial connection.
//
// This example:
//   * doesn't use the String object
//   * converts with the strtol() library function
//   * has no error notification
//*****************************************************************

void setup()
{
  Serial.begin(115200);
  delay(2500);  // to ensure the following "READY" is seen

  // this helps us if the board crashes and restarts
  Serial.println("READY");
}

// allow at most 16 binary digits
const int MAX_DIGITS = 16;
char buffer[MAX_DIGITS + 2];

void loop()
{
  if (Serial.available() > 0)
  {
    int len = Serial.readBytesUntil('\n', buffer, MAX_DIGITS); 

    buffer[len] = '\0';
    unsigned long dec = strtol(buffer, NULL, 2);
    Serial.println(dec);

    // flush unread characters
    delay(10);    // needed, see README
    
    while (Serial.read() != -1)
    {
      ;
    }
  }
}
