#include <SPI.h>
#include <Adafruit_NeoPixel.h>
#include "schedule.h"

///////////////////////////////////////////////////////
// Test code to show fadein/fadeout at various times.
// The times will eventually be at certain hours of the day,
// but initially will be at certain seconds in the minute.
//
// Uses a "schedule" approach, code in files schedule.*.
///////////////////////////////////////////////////////

// Attach a NeoPixel strip Din to the DataPin.
// Change NumLeds to the number of LEDs in the strip
const int DataPin = D6;
const int NumLeds = 8;
const int DisplayBrightness = 32; // display brightness

// define the pixels
Adafruit_NeoPixel pixels(NumLeds, DataPin, NEO_GRB + NEO_KHZ800);

// schedule data
Schedule schedule(NULL, 2);

// initial LED colours, etc
const long FadeInterval = 50;  // when fading change colours every 50msec
int curr_red = 0;
int curr_green = 0;
int curr_blue = 0;

// the previous second value, used to do things when second value changes
int prev_second = 0;

//*********************************************************
// Set all pixels to the given RGB values.
//*********************************************************

void set_all_pixels(int r, int g, int b)
{
  for (int i=0; i < NumLeds; ++i)
  {
    pixels.setPixelColor(i, pixels.Color(r, g, b));
  }
  pixels.show();
}

//*********************************************************
// Clamp a colour byte into [0, 255].
//
// curr  current colour value
// inc   amount to bump colour
//*********************************************************

int bump_colour(int curr, int inc)
{
  curr += inc;
  if (curr < 0)
    curr = 0;
  else if (curr > 255)
    curr = 255;

  return curr;
}

//*********************************************************
// The "fade to GREEN" handler.
//
// If not yet fully faded, change colours and reschedule,
// else report end of fade and don't reschedule.
//*********************************************************

void fade_to_green(void)
{
  // fade a bit more towards GREEN and update LEDS
  curr_red = bump_colour(curr_red, -5);
  curr_green = bump_colour(curr_green, 5);
  curr_blue = bump_colour(curr_blue, -5);
  set_all_pixels(curr_red, curr_green, curr_blue);
  
  // if we haven't finished fading, reschedule
  if (curr_green < 255)
  {
    schedule.schedule(FadeInterval, fade_to_green);
  }
  else  // report
  {
    Serial.printf("end GREEN fade: %d,%d,%d\n", curr_red, curr_green, curr_blue);
    // don't schedule anyting, test "on_empty()"
  }
}

//*********************************************************
// The "fade to BLUE" handler.
//
// If not yet fully faded, change colours and reschedule,
// else report end of fade and don't reschedule.
//*********************************************************

void fade_to_blue(void)
{
  // fade a bit more towards BLUE and update LEDS
  curr_red = bump_colour(curr_red, -5);
  curr_green = bump_colour(curr_green, -5);
  curr_blue = bump_colour(curr_blue, 5);
  set_all_pixels(curr_red, curr_green, curr_blue);
  
  // if we haven't finished fading, reschedule
  if (curr_blue < 255)
  {
    schedule.schedule(FadeInterval, fade_to_blue);
  }
  else  // report
  {
    Serial.printf("end BLUE fade: %d,%d,%d\n", curr_red, curr_green, curr_blue);
    schedule.schedule(FadeInterval, fade_to_green);
  }
}

//*********************************************************
// The "fade to RED" handler.
//
// If not yet fully faded, change colours and reschedule,
// else report end of fade and don't reschedule.
//*********************************************************

void fade_to_red(void)
{
  // fade a bit more towards RED and update LEDS
  curr_red = bump_colour(curr_red, 5);
  curr_green = bump_colour(curr_green, -5);
  curr_blue = bump_colour(curr_blue, -5);
  set_all_pixels(curr_red, curr_green, curr_blue);
  
  // if we haven't finished fading, reschedule
  if (curr_red < 255)
  {
    schedule.schedule(FadeInterval, fade_to_red);
  }
  else  // report
  {
    Serial.printf("end RED fade: %d,%d,%d\n", curr_red, curr_green, curr_blue);
    schedule.schedule(FadeInterval, fade_to_blue);
  }
}

//*********************************************************
// Start the fader chain again.
//
// If not yet fully faded, change colours and reschedule,
// Called from the "on_empty" function in the schedule library.
//*********************************************************

void start_fader_chain(void)
{
  Serial.println("start_fader_chain: restarting chain");
  schedule.schedule(FadeInterval, fade_to_red);
}

//*********************************************************
// Setup
//*********************************************************

void setup()
{
  Serial.begin(115200);

  // initialize the NeoPixel strip
  pixels.begin();
  pixels.setBrightness(DisplayBrightness);  // set "static" brightness
  set_all_pixels(curr_red, curr_green, curr_blue);

  // set the schedule "on empty" handler
  schedule.on_empty(start_fader_chain);
  
  // don't schedule anything, rely on "on empty" handler to do that
  //schedule.schedule(FadeInterval, fade_to_red);

  Serial.println("Start");
}

//*********************************************************
// Every second check if at a fade second (0, 15, 30, 45).
// If so, schedule the appropriate fade handler.
//
// Always call schedule.tick() to process active handlers.
//*********************************************************

void loop()
{
  unsigned long now = millis();
  int second = (now / 1000) % 60;
  
  // perform any scheduled activity
  schedule.tick();

  // do something every second
  if (second != prev_second)
  {
    Serial.println(second);
    prev_second = second;
  }
}
