//---------------------------------------------------------
// A "template" application.
//
// Shows use of DEBUG to control printing and uses a
// printf-style function to make printing more convenient.
//
// Also shows usage of the "blink without delay" idea and
// various other little tricks.
//---------------------------------------------------------

#include <stdarg.h>


// uncomment the next line to turn on debug output
#define	DEBUG

// how long between prints
const unsigned long PrintDelay = 1000;

// how long between LED_BUILTIN flashes
const unsigned long FlashDelay = 123;

//---------------------------------------------------------
// The DEBUG controlled stuff
//---------------------------------------------------------

#ifdef DEBUG
  #define dprintf(...) debug_printf(__VA_ARGS__)

  // like the C printf() function, but always ends with a newline.
  // doesn't handle floats!  arduino limitation?
  
  void debug_printf(char *fmt, ...)
  {
    va_list ap;
    char buff[128];
  
    va_start(ap, fmt);
    vsprintf(buff, fmt, ap);
    Serial.println(buff);
    va_end(ap);
  }
#else
  // turn off any call to dprintf(), generates no code
  #define dprintf(...)
#endif


//---------------------------------------------------------
// normal arduino code
//---------------------------------------------------------

void setup()  
{
  Serial.begin(115200);
  dprintf("READY");

  pinMode(LED_BUILTIN, OUTPUT);
}  
    
void loop()  
{
  // the "blink without delay" last time values
  static unsigned long last_print = 0;
  static unsigned long last_flash = 0;
  
  unsigned long now = millis();
  
  if ((now - last_print) >= PrintDelay)
  {
    last_print = now;
    dprintf("loop: millis()=%ld", now);
  }
  
  if ((now - last_flash) >= FlashDelay)
  {
    // show that we can toggle an OUTPUT pin using digitalRead()
    
    last_flash = now;
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }
}  
